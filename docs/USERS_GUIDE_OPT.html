<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html>

<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<link href="style.css" rel="stylesheet" type="text/css">
<title>User's guide MondriaanOpt</title>
</head>

<body>

<h2>User's guide MondriaanOpt</h2>

<div id="top">
<div><a href="USERS_GUIDE.html">&laquo; Mondriaan</a></div>
<div><a href="#inst">Installing</a></div>
<div><a href="#outp">Output</a></div>
<div><a href="#opts">Options</a></div>
<div><a href="#matl">MATLAB</a></div>
</div>

<hr>
<p>
This page is continuously being improved and updated;
therefore, a more recent version may be obtained 
<a href="http://www.staff.science.uu.nl/~bisse101/Mondriaan/Docs/USERS_GUIDE_OPT.html">
online</a>.
This offline version is bundled with the software for your convenience.
</p>
<hr>

<p>
Whereas Mondriaan uses heuristics to obtain good partitionings for sparse matrix-vector multiplication for any number of processors,
MondriaanOpt will calculate an actual optimal solution for this partitioning problem with 2 processors. More precisely, it will
calculate a partitioning with minimum volume among all solutions that obey the imbalance constraint.
</p>

<p>
A database with already solved problems with use of MondriaanOpt can be found <a href="http://www.staff.science.uu.nl/~bisse101/Mondriaan/Opt/">online</a>.
</p>

<h3><a name="inst">How to install MondriaanOpt</a></h3>
<p>
MondriaanOpt comes packaged with the Mondriaan software. Refer to <a href="./USERS_GUIDE.html">this page</a> for
instructions on using Mondriaan. MondriaanOpt is automatically compiled when you compile Mondriaan. The executable
is then available at <tt>tools/MondriaanOpt</tt>.
</p>

<h3><a name="run">How to run MondriaanOpt</a></h3>
<p>
The MondriaanOpt program has the following interface:
<ul><li><tt>% ./tools/MondriaanOpt matrix [P [eps]] [options]</tt></li></ul>
One, two or three parameters may be passed, after which further options may be given.
Either [eps], -e or -k must be passed, and it is advised to pass -v (see <a href="#opts">options</a>).
Take note that while MondriaanOpt may be called with the same parameters as Mondriaan, the actual problem
being solved may be <a href="#constraint">slightly different</a>.
</p>

<p>
Some equivalent examples are:
</p>
<ul>
	<li><tt>% ./tools/MondriaanOpt tests/arc130.mtx 2 0.03 -v 17</tt></li>
	<li><tt>% ./tools/MondriaanOpt tests/arc130.mtx -e 0.03 -v 17</tt></li>
	<li><tt>% ./tools/MondriaanOpt tests/arc130.mtx -k 660 -v 17</tt></li>
</ul>

<p>
The above examples partition the <tt>arc130.mtx</tt> matrix (Matrix Market file format)
for 2 processors with at most 3% load imbalance, knowing that solutions must exist with
volume at most 17. The matrix should be the full relative path; <em>in the above example 
output is saved in the Mondriaan tests folder</em> (<tt>../tests/</tt>).
</p>

<h3><a name="outp">Output</a></h3>

<p>The <tt>MondriaanOpt</tt> tool yields, after a successful run on an input matrix,
various output files. All possible output files are described below. Typically,
the output filenames are that of the input matrix filename, modified with a small
descriptor and the number of parts <i>(=2)</i>.</p>

<h4><u>Formats with free nonzeros</u></h4>
<p><i>
All assigned nonzeros are assigned a processor-number to be assigned to, either 1 or 2.
All free nonzeros will be assigned index 3.
(Free nonzeros are nonzeros that are not assigned to a processor because assigning
it to either one will not influence communication volume.)
To stress the potential presence of free nonzeros, the number of processors (2) in the
filename is followed by a suffix <tt>f</tt>.
</i></p>

<h4>Processor indices (<tt>-I2f</tt>)</h4>
<p> The <tt>MondriaanOpt</tt> program writes the processor indices of each
nonzero to the Matrix Market file <tt>input-2f.mtx</tt> where the value of each
nonzero is replaced by the processor index to which the nonzero has been assigned.
</p>

<h4>Graphical output (<tt>-2f.svg</tt>)</h4>
<p>If the option <tt>-svg</tt> is given, at the end of the algorithm an SVG graphic is written to the file <tt>input-2f.svg</tt>,
containing a visualisation of the partitioning.
</p>

<h4><u>Formats without free nonzeros</u></h4>
<p><i>
Here, the free nonzeros of a partitioning are distributed among the two processors in
such a way that load imbalance is kept at a minimum.
Note that whenever we write <tt>P</tt> for the number of processors below, it implicitly equals 2.
</i></p>
<h4>Distributed matrix (<tt>-P2</tt>)</h4>
<p> The <tt>MondriaanOpt</tt> program
writes the distributed matrix to a file called <tt>input-P2</tt>,
where <tt>input</tt> is the name of the input matrix.

We use an adapted Matrix Market format, with this structure: 
<br>
<tt>%%MatrixMarket distributed-matrix coordinate real general<br>
m n nnz P<br>
Pstart[0]</tt> ( this should be 0 )<br>
...<br>
...<br>
...<br>
<tt>Pstart[P]</tt>( this should be nnz )<br>
<tt>A.i[0] A.j[0] A.value[0]</tt>
...<br>
...<br>
...<br>
<tt>A.i[nnz-1] A.j[nnz-1] A.value[nnz-1]</tt>
<br>
Here, <tt>Pstart[k]</tt> points to the start of the nonzeroes
of processor k.
</p>

<h4>Processor indices (<tt>-I2</tt>)</h4>
<p> The <tt>MondriaanOpt</tt> program
also writes the processor indices of each nonzero to the Matrix Market file <tt>input-I2</tt>
where the value of each nonzero is replaced by the processor index to which
the nonzero has been assigned. The order of the nonzeroes is exactly that of the distributed matrix (<tt>-P2</tt>).
</p>

<h4>Cartesian submatrices (<tt>-C2</tt>)</h4>
<p> The program writes the row index sets I(q) 
and column index sets J(q) of the Cartesian submatrix I(q) x J(q)
for the processors q=1,...,P to the file called <tt>input-C2</tt>.
This file is additional information, useful e.g. for visualisation,
and you may not need it.
</p>

<h4>Graphical output (<tt>-2.svg</tt>)</h4>
<p>If the option <tt>-svg</tt> is given, at the end of the algorithm an SVG graphic is written to the file <tt>input-2.svg</tt>,
containing a visualisation of the partitioning.
</p>

<h4><u>Output to <tt>stdout</tt>/<tt>stderr</tt></u></h4>
<p>
In a succesful run, at the end of execution general statistics are written to <tt>stdout</tt>.
Also, during such a run, every <tt>2^23 = 8388608</tt> iterations the current depth in the tree is written to <tt>stderr</tt>, in the format <tt>`current depth`/`maximum depth`</tt>.
Last but not least, every time a new solution is found which improves on the previous solution regarding total volume, a message is written to <tt>stderr</tt> reporting the newly found volume and load distribution in the format <tt>`load P0`, `load P1`, `load Free`</tt>.
</p>

<h3><a name="opts">Program options</a></h3>
<p>
The MondriaanOpt program has the following interface:
<ul><li><tt>% ./tools/MondriaanOpt matrix [P [eps]] [options]</tt></li></ul>
One, two or three parameters may be passed, after which further options may be given.
An overview of the available parameters and options is given below.
</p>
<h4>Parameters</h4>
<table>
	<thead>
		<tr>
			<th>Parameter</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Matrix file</td>
			<td><i>Required.</i> The input matrix file in Matrix Market (.mtx) format</td>
		</tr>
		<tr>
			<td>Number of processors</td>
			<td>Present for consistency with other Mondriaan* commands. This parameter, if given, must be equal to 2.</td>
		</tr>
		<tr>
			<td>Load imbalance</td>
			<td>The maximum allowed load imbalance</td>
		</tr>
	</tbody>
</table>

<h4>Options</h4>
<p>
Apart from the matrix, at least one of [eps], -e or -k must be given, defining the maximum allowed load imbalance.
</p>
<table>
	<thead>
		<tr>
			<th>Option</th>
			<th>Value</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>-v</td>
			<td>Volume</td>
			<td>
				<i>Recommended.</i> The starting upper bound volume.
				This defaults to <tt>min(m,n)+1</tt>, with <tt>m</tt> and <tt>n</tt> denoting the dimensions of the matrix to be partitioned.
				While this is a valid upper bound, you may wish to pass a tighter upper bound to reduce computing time.
			</td>
		</tr>
		<tr>
			<td>-e</td>
			<td>Load imbalance</td>
			<td>The maximum allowed load imbalance</td>
		</tr>
		<tr>
			<td>-k</td>
			<td>Number of nonzeros</td>
			<td>The maximum allowed number of nonzeros per part</td>
		</tr>
		<tr>
			<td>-t</td>
			<td>Seconds</td>
			<td>Max running time in seconds</td>
		</tr>
		<tr>
			<td>-h</td>
			<td><i>None</i></td>
			<td>Show help</td>
		</tr>
		<tr>
			<td>-svg</td>
			<td><i>None</i></td>
			<td>Write visualisations of the partitioning to <tt>.svg</tt> files</td>
		</tr>
	</tbody>
</table>

<h3><a name="constraint">Difference in imbalance constraints</a></h3>
While the command line interface of MondriaanOpt can be used just as Mondriaan, there is a subtle difference in the problem being solved in these two.
More precisely, with <tt>N</tt> being the total number of nonzeros, <tt>p</tt> being the total number of processors (which equals 2) and
<tt>load</tt> the number of nonzeros assigned to a processor, compare:
<ul>
	<li>the imbalance constraint <tt>load &lt;= (1+epsilon) (N/p)</tt> which is used in Mondriaan (In the code, this amounts to <tt>floor( ((1+epsilon)*N)/p )</tt>.), and</li>
	<li>the imbalance constraint <tt>load &lt;= (1+epsilon) ceil(N/p)</tt> which is used in MondriaanOpt.</li>
</ul>
As <tt>p=2</tt>, this difference may only be of importance whenever <tt>N</tt> is odd.
In [<a href="#cite1">1,p.2</a>] it is explained that this different choice was made to ensure feasibility of the problem, even if <tt>epsilon=0</tt>.


<h3><a name="matl">Using MondriaanOpt in MATLAB</a></h3>

<p>
For more information about MATLAB usage, please see the <a href="MATLAB.html">Mondriaan MATLAB guide</a>.
MondriaanOpt is available in Matlab using the <tt>MatlabMondriaanOpt</tt> MEX routine. Example matlab files
are given as <tt>mondriaanOpt.m</tt> and <tt>mondriaanOptPlot.m</tt>.
</p>


<h3>References</h3>
<p>
[<a name="cite1" href="http://doi.org/10.1016/j.jpdc.2015.06.005">1</a>]
<em>An exact algorithm for sparse matrix bipartitioning</em>,
Daniel M. Pelt and Rob H. Bisseling, <i>Journal of Parallel and Distributed Computing</i>, <b>85</b> (2015) pp. 79-90.
</p>

<hr>
<p>
Last updated: November 4, 2016.<br><br>
November 4, 2016 by Marco van Oort.<br><br>
To <a href="http://www.staff.science.uu.nl/~bisse101/Mondriaan">
the Mondriaan package home page</a>.</p>

<p>
<a href="http://validator.w3.org/check?uri=referer">
<img style="border:0;width:88px;height:31px" src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01 Strict">
</a>
<a href="http://jigsaw.w3.org/css-validator/check/referer">
<img style="border:0;width:88px;height:31px" src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!">
</a>
</p>

<hr>

</body>

</html>

